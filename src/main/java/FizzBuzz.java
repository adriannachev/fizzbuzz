public class FizzBuzz {

    String printFizzBuzz(int number) {
        String output = "";
        if (number % 3 == 0 || Integer.toString(number).contains("3")) {
            output += "Fizz";
        }
        if (number % 5 == 0 || Integer.toString(number).contains("5")) {
            output += "Buzz";
        }
        if (output.equals("")) {
            output = Integer.toString(number);
        }
        System.out.println(output);
        return output;
    }

    public static void main(String[] args) {
        FizzBuzz fizzBuzz = new FizzBuzz();
        for (int i = 1; i <= 100; i++) {
            fizzBuzz.printFizzBuzz(i);
        }
    }
}
