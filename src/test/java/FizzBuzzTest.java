import org.assertj.core.api.Assertions;
import org.junit.Test;

public class FizzBuzzTest {

    @Test
    public void testPrintFizzBuzz() {
        FizzBuzz fizzBuzz = new FizzBuzz();

        Assertions.assertThat(fizzBuzz.printFizzBuzz(1)).isNotNull();
        Assertions.assertThat(fizzBuzz.printFizzBuzz(1)).isEqualTo("1");
        Assertions.assertThat(fizzBuzz.printFizzBuzz(2)).isEqualTo("2");
        Assertions.assertThat(fizzBuzz.printFizzBuzz(3)).isEqualTo("Fizz");
        Assertions.assertThat(fizzBuzz.printFizzBuzz(5)).isEqualTo("Buzz");
        Assertions.assertThat(fizzBuzz.printFizzBuzz(15)).isEqualTo("FizzBuzz");
        Assertions.assertThat(fizzBuzz.printFizzBuzz(52)).isEqualTo("Buzz");
        Assertions.assertThat(fizzBuzz.printFizzBuzz(31)).isEqualTo("Fizz");
        Assertions.assertThat(fizzBuzz.printFizzBuzz(53)).isEqualTo("FizzBuzz");
        Assertions.assertThat(fizzBuzz.printFizzBuzz(54)).isEqualTo("FizzBuzz");
        Assertions.assertThat(fizzBuzz.printFizzBuzz(100)).isEqualTo("Buzz");
    }
}